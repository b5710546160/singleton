#Singleton pattern
The singleton pattern is the pattern is create only one object. it used when we want to use only one object in many class. It helps ours programs can share infomation by use this pattern to send data.

####Advantages
- control access to a single instance.
- reduce name space pollution - better than using a global variable.
- permits refinement of operations and representation.
- permits a variable number of instances - you can modify the singleton to produce more than one instance.
- more flexible than class operations.

####Disadvantages
- Singleton cannot be subclassed, since the constructoris private and static getInstance() is not polymorphic.

##Example 

Singleton pattern has three type.
    
###1.Lazy initialization

This type that we create a resource only when it is needed.
This avoids creating something that may never be used.

    public class Singleton {
        private static Singleton instance = null;
 
        private Singleton() {}
 
        public static Singleton getInstance() {
            if (instance == null) {
                instance = new Singleton();
            }
 
            return instance;
        }
 
        public void doWork() {
            // do something
        }
    }
     
private static Singleton instance = null;

- private static attribute that is the only instance of this class

private Singleton() {}

- constructor is private to prevent other classes from creating objects 


public static Singleton getInstance() {}

-  public static accessor returns the single instance of this class.

- instance is created the first time that getInstance( ) is called, but not before.  

- If getInstance is never called, no Singleton is created.

public void doWork() {}

- public void for use instance.


####Code for other object get the Singleton
    Singleton  singleton = Singleton.getInstance();
    
#####Code for work

    Singleton.getInstance().doWork();

###2.Eager initialization

This type that we alway create a resource by no care we want to use it or not.

    public class Singleton {
        private static final Singleton instance = new Singleton();
 
        private Singleton() {}
 
        public static Singleton getInstance() {
            return INSTANCE;
        }
        public void doWork() {
            // do something
        }
    }

private static final Singleton instance = new Singleton();

- private instance alway created when we use class.

public static Singleton getInstance() {}

- public static accessor returns the single instance of this class.

public void doWork() {}

- public void for use instance.

####Code for other object get the Singleton
    Singleton  singleton = Singleton.getInstance();
    
#####Code for work

    Singleton.getInstance().doWork();
    

###3. Enum initialization

This type is used by enum. It has only one object by can not create more. 

    public enum Singleton{
    INSTANCE;
    public void doWork() {
            // do something
        }
    }
   
#####Code for work

    Singleton.INSTANCE.doWork();
    
###Exercise

######1. What is Singleton pattern?

……………………………………………………………………………………………………..........

……………………………………………………………………………………………………………

######2. Change this code to use Singleton pattern.


        public class Singleton {
	
	private Singleton instance;
	
	public Singleton() {}
	
	public Singleton getInstance() {

    instance = new Singleton();
    
    return instance;
    
    }
	
	public void doWork() {
     
    // do something
    
    }

    }
   
   ...................................................................................................................
   
   ...................................................................................................................
   
   ...................................................................................................................
   
   ...................................................................................................................
   
...................................................................................................................
   
   ...................................................................................................................

...................................................................................................................
   
   ...................................................................................................................
   
   ...................................................................................................................
   
   ...................................................................................................................
   
...................................................................................................................
   
   ...................................................................................................................
   
   ...................................................................................................................
   
   ...................................................................................................................
   
   ...................................................................................................................
   
   ...................................................................................................................
   
...................................................................................................................
   
   ...................................................................................................................


######3. In the type of Singleton, what type of Singleton pattern you will choose in code? Explain why?

   ……………………………………………………………………………………………………..........

……………………………………………………………………………………………………………

######4.  What is advantages of Singleton pattern?
    
……………………………………………………………………………………………………..........
   
……………………………………………………………………………………………………………
    
……………………………………………………………………………………………………..........
    
……………………………………………………………………………………………………………


######5. What is disadvantages of Singleton pattern?
    
……………………………………………………………………………………………………..........

……………………………………………………………………………………………………………


Author

  Kitipoom Kongpetch 5710546160